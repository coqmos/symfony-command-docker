<?php

namespace TestTask\Serializer\PropertyInfo;


class ApiDataPropertyInfo
{

    //region Attributes
    private $uid;
    private $id;
    private $countries;
    private $country;
    private $rvs;
    private $partner;
    private $price;
    private $tokens;
    private $shop;
    //endregion

    //region Getters & Setters

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     * @return $this;
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return $this;
     */
    public function setId($id)
    {
        $this->id = $id;
        $this->setUid($id);
        return $this;
    }



    /**
     * @return mixed
     */
    public function getRvs()
    {
        return $this->rvs;
    }

    /**
     * @param mixed $rvs
     * @return $this;
     */
    public function setRvs($rvs)
    {
        $this->rvs = ($rvs)?'yes':((!$rvs)?'no':'not set');
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * @param mixed $countries
     * @return $this;
     */
    public function setCountries($countries)
    {
        $this->countries = (is_array($countries))?implode(',',$countries):$countries;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * @param mixed $partner
     * @return $this;
     */
    public function setPartner($partner)
    {
        $this->partner = $partner;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     * @return $this;
     */
    public function setPrice($price)
    {
        $this->price = (is_object($price) && property_exists($price,'amount') )?$price->amount:0;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     * @return $this;
     */
    public function setCountry($country)
    {
        $this->country = $country;
        $this->setCountries([$country]);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTokens()
    {
        return $this->tokens;
    }

    /**
     * @param mixed $tokens
     * @return $this;
     */
    public function setTokens($tokens)
    {
        $this->tokens = $tokens;
        $price = new \stdClass();
        $price->amount = $tokens/500;
        $this->setPrice($price);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param mixed $shop
     * @return $this;
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
        $this->setPartner($shop);
        return $this;
    }


    //endregion

}