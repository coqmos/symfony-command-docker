<?php
/**
 * Created by PhpStorm.
 * User: coqmo
 * Date: 6/6/2018
 * Time: 8:36 AM
 */

namespace TestTask\Serializer\Normalizer;

use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use TestTask\Entity\Product;


class ProductNormalizer implements NormalizerInterface
{

    public function normalize( $object, $format = null, array $context = array())
    {



        $product = new Product();
        //$product->setId($uuid);
        $product->setProductExternalId($object->getUid());
        $product->setCountries($object->getCountries());
        $product->setPrice($object->getPrice());
        $product->setPartner($object->getPartner());
        $product->setRvs($object->getRvs());

        return $product;
    }

    public function supportsNormalization($data, $format = null)
    {
        return is_object($data);
    }

}