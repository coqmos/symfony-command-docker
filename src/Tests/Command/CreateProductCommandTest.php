<?php
/**
 * Created by PhpStorm.
 * User: coqmo
 * Date: 6/6/2018
 * Time: 12:59 AM
 */

namespace TestTask\Tests\Command;


use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use TestTask\Command\CreateProductCommand;
use Symfony\Component\Console\Application;

class CreateProductCommandTest extends KernelTestCase
{

    public function testExecute(){
        $kernel = static::createKernel();
        $kernel->boot();

        $application = new Application($kernel);
        $application->add(CreateProductCommand::class);

        $command = $application->find('app:create-product');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'command'=> $command->getName()
        ]);
        $output = $commandTester->getDisplay();
        $this->assertContains('Products successfully generated!', $output);
    }

}