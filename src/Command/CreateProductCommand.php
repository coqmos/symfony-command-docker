<?php

namespace TestTask\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TestTask\Service\ProductService;

class CreateProductCommand extends Command
{
    private $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('app:create-products')
            ->setDescription('Creates products')
            ->setHelp('This command allows you to create products');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Product Create Running',
            '===============',
        ]);
        $this->productService->create();

        $output->writeln('Products successfully generated!');

    }

}