<?php
/**
 * Created by PhpStorm.
 * User: coqmo
 * Date: 6/6/2018
 * Time: 12:54 AM
 */

namespace TestTask\Service;

use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use TestTask\Entity\Product;
use TestTask\Serializer\PropertyInfo\ApiDataPropertyInfo;
use TestTask\Serializer\Normalizer\ProductNormalizer;

class ProductService
{
    protected  $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }


    public function create(){


        $prodNormalizer = new ProductNormalizer();
        $refNormalizer = new ObjectNormalizer(null,null,null,new ReflectionExtractor());
        $serializer = new Serializer(array($refNormalizer));
        $serializer2 = new Serializer(array($prodNormalizer));

        $data =json_decode( file_get_contents(getenv('PARTNER_API')));

        $batchSize = 20;
        $count     = 0;
        foreach ($data as $itemSet){

            foreach ($itemSet as $i => $item){
                $jsonContent = $serializer->denormalize($item,ApiDataPropertyInfo::class);
                $product = $serializer2->normalize($jsonContent,'json');

                $this->em->persist($product);
                $count ++;

                if (($i % $batchSize) == 0) {
                    $this->em->flush();
                    $this->em->clear();
                }

            }

        }
        // flush the remaining objects
        $this->em->flush();
        $this->em->clear();

        return $count;
    }

}