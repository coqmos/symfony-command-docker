<?php

namespace TestTask\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @ORM\Table(name="product")
 */
class Product
{
    //region Attributes
    /**
     * @var \Ramsey\Uuid\UuidInterface
     *
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidGenerator")
     */
    private $id;

    /**
     * @ORM\Column(type="guid")
     */
    private $product_external_id;

    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @var string
     */
    private $countries;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2)
     */
    private $price;

    /**
     * @ORM\Column(type="string")
     */
    private $partner;

    /**
     *  @ORM\Column(name="rvs", type="string", columnDefinition="enum('yes', 'no', 'not set')")
     */
    private $rvs;
    //endregion


    //region Getters & Setters


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return $this;
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProductExternalId()
    {
        return $this->product_external_id;
    }

    /**
     * @param mixed $product_external_id
     * @return $this;
     */
    public function setProductExternalId($product_external_id)
    {
        $this->product_external_id = $product_external_id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * @param mixed $countries
     * @return $this;
     */
    public function setCountries($countries)
    {
        $this->countries = $countries;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     * @return $this;
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPartner()
    {
        return $this->partner;
    }

    /**
     * @param mixed $partner
     * @return $this;
     */
    public function setPartner($partner)
    {
        $this->partner = $partner;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRvs()
    {
        return $this->rvs;
    }

    /**
     * @param mixed $rvs
     * @return $this;
     */
    public function setRvs($rvs)
    {
        $this->rvs = $rvs;
        return $this;
    }

    //endregion
}
